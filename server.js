const $express = require('express');
const $config = require('config');
const app = $express();

const $PostValidation = require('./src/modules/post-validator');

require('./src/middlewares/body-parser')(app);

app.post('/post', (req, res) => {
  const text = req.body.text || '';

  if (!text) {
    return res.json({ok: false});
  }

  const result = $PostValidation.check(text);

  res.json(result);
});

const PORT = $config.get('server.port');

app.listen(PORT, () => {
  console.log('DTP Text Validator listing :%s port', PORT);
});
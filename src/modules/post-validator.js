const $crc32 = require('crc-32');
const $escapeRegExp = require('escape-string-regexp');

class PostValidator {
  static get MIN_POST_LENGTH () {
    return 270;  
  }

  static check (text) {
    const textSize = text.length;
    const floodCalc = text
      .match(/([a-zА-Яа-я0-9ёЁèé'-])+/gi)
      .join(' ')
      .match(/(([a-zА-Яа-я0-9ёЁèé'-])+\s){2}/gi)
      .reduce((calc, item) => {
        const partHash = $crc32.str(item);
        const length = item.length;
        
        if (!calc[partHash]) {
          calc[partHash] = {
            repeat: 0,
            length
          };
        } else {
          ++calc[partHash].repeat;
        }

        return calc;
      }, {});

    const floodSize = Object.keys(floodCalc).reduce((size, key) => {
      const item = floodCalc[key];

      if (item.repeat > 0) {
        size += item.length * item.repeat;
      }

      return size;
    }, 0);

    const flood = Math.round(floodSize / (textSize / 100));
    const normalLength = textSize >= PostValidator.MIN_POST_LENGTH;

    return (flood < 20 && normalLength);
  }
}

module.exports = PostValidator;